/**
Sample program using the Google API and distancematrix
Also provides an example of Java's JSON object

@author Gary Lewandowski
Fall 2016
@version 10Aug2016
**/


import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonArray;
import java.net.MalformedURLException;
import java.security.cert.Certificate;
import java.net.URL;
import java.io.*;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLPeerUnverifiedException;


public class GoogleTest
{
  // java programs we want to run externally (e.g., not in BlueJ) must have main
  public static void main(String [] args)
  {
    // This is an example of the google api string for the distancematrix
    // you'll replace the origin with cincinnati and the destination with the destination
    String url="https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=Disneyland&destinations=Universal+Studios+Hollywood4&key=AIzaSyDBA7zWtPfqzddtEYdsgwfV24g09ttkXmc";
    new GoogleTest(url); // constructs the GoogleTest object. The constructor does the rest
  }

/**
* GoogleTest constructor
* @param String urlString is the googleapi string
* @returns nothing, but outputs to the console the distance between the locations
**/
  public GoogleTest(String urlString)
  {
    URL url;
    try {
      // construct a URL
      url = new URL(urlString);
      // Open the connection across the network; receive the response
      HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
      System.out.println("Response code: " + con.getResponseCode());
      System.out.println(con.getResponseMessage());
      // the InputStream is the data passed back from the url
      // the next three lines set up a way to read the data
      InputStream ins = con.getInputStream();
      InputStreamReader isr = new InputStreamReader(ins);
      BufferedReader in = new BufferedReader(isr);

      String inputLine;
      String jsonString="";

      while ((inputLine = in.readLine()) != null) // read each line
      {
        //System.out.println(inputLine);
        jsonString += inputLine;
      }

      in.close();
      useJSONToFindDistance(jsonString);
    }
    catch (MalformedURLException e) { e.printStackTrace();}
    catch (IOException e) {e.printStackTrace();}
  }


  public void useJSONToFindDistance(String jsonString)
  {
    /**
    A JSON object consists of named pairs. The JSON we get back from the distance
    matrix api is nested a bit so this pulls out the information in a step-by-
    step fashion
    **/

    // first create the JsonObject so we can easily access the data
      JsonObject jsonObj = Json.createReader(new StringReader(jsonString)).readObject();
      //System.out.println(jsonObj.toString());

      // The distancematrix is stored in an array attached to the name rows
      JsonArray distancematrix = jsonObj.getJsonArray("rows"); // lame name!

      // The distance information is stored as the first item in that array
      JsonObject distanceInfo = distancematrix.getJsonObject(0);
      // distanceInfo is an object holding an array of elements objects
      // elements is an object inside of distanceInfo holding an array of distance objects
      JsonArray elements = distanceInfo.getJsonArray("elements");

      // The information we want is in the first item of the array
      JsonObject distanceAndDuration = elements.getJsonObject(0);
      // and specifically we want the distance from that item
      JsonObject distance = distanceAndDuration.getJsonObject("distance");
      // the distance is stored in readable form under the name "text"
      System.out.println("distance " + distance.getString("text"));
  }
}
