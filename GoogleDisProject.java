/**
 *CSCI 220-02 project #1
 *this program should produce distances between the given cities and
 *cincinnati and in turn create a list of cities and their distance to
 *this city and sort them nearest to furthest
 *The current cities are (Indianapolis, Chicago, Pittsburgh, Louisville, Cleveland)
 *@author: Brennan Mitchell
 *
 *@edit. 09/11/16
 **/

 import javax.json.Json;
 import javax.json.JsonObject;
 import javax.json.JsonReader;
 import javax.json.JsonArray;
 import java.net.MalformedURLException;
 import java.security.cert.Certificate;
 import java.net.URL;
 import java.io.*;
 import java.util.List;
 import java.util.Scanner;
 import javax.net.ssl.HttpsURLConnection;
 import javax.net.ssl.SSLPeerUnverifiedException;

public class GoogleDisProject
{
public static void main(String [] args)
{
run();
}
/**
run() is the main processing algorithm for GoogleDisProject.
The destination array initially holds the url that is used to access the
googleAPI. The url is then replaced with the JsonString from the googleAPI.
Then desination array is sorted from closest to furthest from Cincinnati.
Finally a string is compiled for each destination and retured in order from
nearest to furthest
**/

public static void run()
{
  String[] destination = new String[5];
  destination[0] = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=Indianapolis&destinations=Cincinnati&key=AIzaSyDBA7zWtPfqzddtEYdsgwfV24g09ttkXmc";
  destination[1] = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=Chicago&destinations=Cincinnati&key=AIzaSyDBA7zWtPfqzddtEYdsgwfV24g09ttkXmc";
  destination[2] = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=Pittsburgh&destinations=Cincinnati&key=AIzaSyDBA7zWtPfqzddtEYdsgwfV24g09ttkXmc";
  destination[3] = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=Louisville&destinations=Cincinnati&key=AIzaSyDBA7zWtPfqzddtEYdsgwfV24g09ttkXmc";
  destination[4] = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=Cleveland&destinations=Cincinnati&key=AIzaSyDBA7zWtPfqzddtEYdsgwfV24g09ttkXmc";

  for (int i = 0; i < destination.length; i++)
  {
    if (destination[i] != null)
    {
      //this loop exchanges the url in destination[] for a JsonString from googleAPI
      String site = destination[i];
      destination[i] = GoogleDistanceProject(site);
    }
  }
  for (int i = 0; i < (destination.length - 1); i++)
  //sorting algorithm for destination array
  {
    if (destination[i] != null && destination[i+1] != null)
    {
      int z = i;
      boolean bigger = true; // set to false when destination < i is ordered
      while (z >= 0 && bigger == true)
      {
        /**
        compairs two destinations at a time and switches their location if the
        first is greating than the second
        will repeat until the list from i to 0 is sorted
        **/
        int x = JsonMeasure(destination[z]);
        int y = JsonMeasure(destination[(z+1)]);
        if (x > y)
        {
          String holder = destination[z];
          destination[z] = destination[(z+1)];
          destination[(z+1)] = holder;
        }else{
          bigger = false;
        }
        z--;
      }
    }
  }
  for (int i = 0; i < destination.length; i++)
  {
    /**
    compiles a string to return to the user
    stringA = City, State, Country
    stringB = the distance in miles
    hence the final string should look like:
    Louisville, KY, USA is 100 mi from Cincinnati, OH, USA
    **/
    String stringA = JsonLocation(destination[i]);
    String stringB = JsonDistance(destination[i]);
    String city = (stringA + " is " + stringB + " from Cincinnati, OH, USA");
    System.out.println (city);
  }
}
/**
GoogleDistanceProject takes in a url from the destination array and goes onto
compile a JsonString with GoogleAPI and returns the JsonString and it is
in turn stored in the destination array where the url was stored previously
ALGORITHM DERRIVED FROM THE GIVEN EXAMPLE FOR PROJECT 1 but with a return
statement added at the end
@Author: Gary Lewandowski
@Revised by: Brennan Mitchell
**/
public static String GoogleDistanceProject(String site)
{
  URL disLink;
  String jsonString = "";
  try
  {
    disLink = new URL (site);
    HttpsURLConnection con = (HttpsURLConnection) disLink.openConnection();
    System.out.println("Response Code: " + con.getResponseCode());
    System.out.println(con.getResponseMessage());
    InputStream ins = con.getInputStream();
    InputStreamReader isr = new InputStreamReader(ins);
    BufferedReader in = new BufferedReader (isr);
    String inputLine;
    while ((inputLine = in.readLine()) != null)
    {
      jsonString += inputLine;
    }

    in.close();
  }
  catch (MalformedURLException e){e.printStackTrace();
  }
  catch (IOException e){e.printStackTrace();
  }
  return jsonString;
}
/**
JsonMeasure takes a JsonString and returns the numerical distance
from a given city to cincinnati.
This will be used to compair and sort destinations based on their distance to
Cincinnati
**/
public static int JsonMeasure(String jsonString)
{
  JsonObject jsonObj = Json.createReader(new StringReader(jsonString)).readObject();
  JsonArray distancematrix = jsonObj.getJsonArray("rows");
  JsonObject distanceInfo = distancematrix.getJsonObject(0);
  JsonArray elements = distanceInfo.getJsonArray("elements");
  JsonObject distanceAndDuration = elements.getJsonObject(0);
  JsonObject distance = distanceAndDuration.getJsonObject("distance");
  int measure = distance.getInt("value");
  return measure;
}
/**
JsonDistance obtains the distance(string) between a given city and cincinnati
from a JsonString. The produced string will only be used to show a user the
distance(in miles) from a Location to Cincinnati in the final string.
ALGORITHM DERRIVED FROM THE GIVEN EXAMPLE FOR PROJECT 1 but with a return
statement added at the end
@Author: Gary Lewandowski
@Revised by: Brennan Mitchell
**/
public static String JsonDistance(String jsonString)
{
  JsonObject jsonObj = Json.createReader(new StringReader(jsonString)).readObject();
  JsonArray distancematrix = jsonObj.getJsonArray("rows");
  JsonObject distanceInfo = distancematrix.getJsonObject(0);
  JsonArray elements = distanceInfo.getJsonArray("elements");
  JsonObject distanceAndDuration = elements.getJsonObject(0);
  JsonObject distance = distanceAndDuration.getJsonObject("distance");
  return (distance.getString("text"));
}
/**
JsonLocation will take in a JsonString and return the name of the city that
it is compairing to Cincinnati. This will be used when compiling the final
string to returnto a user
**/
public static String JsonLocation(String jsonString)
{
  JsonObject jsonObj = Json.createReader(new StringReader(jsonString)).readObject();
  JsonArray address = jsonObj.getJsonArray("origin_addresses");
  String[] addresses = new String [address.size()];
  String cityName = "";
  for(int i = 0; i < address.size(); i++)
  {
    addresses[i] = address.getString(i);
    cityName = cityName + addresses[i];
  }

  return cityName;
}
}
